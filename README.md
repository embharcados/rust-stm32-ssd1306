# Usage

you can just use `make`
```sh
$ make
```

or if you prefer:

```sh
$ cargo build --release
$ arm-none-eabi-objcopy -O binary target/thumbv7m-none-eabi/release/stm32-ssd1306-display firmware.bin
$ st-flash erase
$ st-flash write firmware.bin 0x8000000
```
