flash:
	cargo build --release
	arm-none-eabi-objcopy -O binary target/thumbv7m-none-eabi/release/stm32-ssd1306-display firmware.bin
	sudo st-flash erase
	sudo st-flash write firmware.bin 0x8000000
