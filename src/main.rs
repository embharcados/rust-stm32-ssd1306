#![no_std]
#![no_main]

use cortex_m_rt::{entry, exception, ExceptionFrame};
use embedded_graphics::{image::Image, pixelcolor::BinaryColor, prelude::*};
use panic_halt as _;
use ssd1306::{prelude::*, Builder, I2CDIBuilder};
use stm32f1xx_hal::{
    delay::Delay,
    i2c::{BlockingI2c, DutyCycle, Mode},
    prelude::*,
    stm32,
};
use tinybmp::Bmp;

const HEIGHT: u32 = 64;

enum Scroll {
    Up,
    Down,
}

#[entry]
fn main() -> ! {
    let dp = stm32::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();
    let clocks = rcc
        .cfgr
        .use_hse(8.mhz())
        .sysclk(72.mhz())
        .pclk1(36.mhz())
        .freeze(&mut flash.acr);
    let mut afio = dp.AFIO.constrain(&mut rcc.apb2);
    let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);
    let mut delay = Delay::new(cp.SYST, clocks);

    let scl = gpiob.pb8.into_alternate_open_drain(&mut gpiob.crh);
    let sda = gpiob.pb9.into_alternate_open_drain(&mut gpiob.crh);

    let i2c = BlockingI2c::i2c1(
        dp.I2C1,
        (scl, sda),
        &mut afio.mapr,
        Mode::Fast {
            frequency: 400_000.hz(),
            duty_cycle: DutyCycle::Ratio2to1,
        },
        clocks,
        &mut rcc.apb1,
        1000,
        10,
        1000,
        1000,
    );

    let interface = I2CDIBuilder::new().init(i2c);
    let mut disp: GraphicsMode<_> = Builder::new().connect(interface).into();

    disp.init().unwrap();

    let mut scroll_direction = Scroll::Down;
    let bmp = Bmp::from_slice(include_bytes!("../ferris.bmp")).unwrap();
    let mut image: Image<Bmp, BinaryColor> = Image::new(&bmp, Point::zero());

    loop {
        // scroll image
        let top_y = image.top_left().y;
        let bottom_y = image.bottom_right().y - HEIGHT as i32;

        if bottom_y <= 0 {
            scroll_direction = Scroll::Up; // reached bottom
            delay.delay_ms(1_000_u16);
        } else if top_y > 0 {
            scroll_direction = Scroll::Down; // reached top
        }

        // translate
        let offset = match scroll_direction {
            Scroll::Down => Point::new(0, -1),
            Scroll::Up => Point::new(0, 1),
        };
        image.translate_mut(offset).draw(&mut disp).unwrap();
        disp.flush().unwrap();
    }
}

#[exception]
fn HardFault(ef: &ExceptionFrame) -> ! {
    panic!("{:#?}", ef);
}
